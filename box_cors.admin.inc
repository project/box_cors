<?php

/**
 * @file
 * Administration form setup for Box CORS Upload.
 */

/**
 * Builds the Admin form.
 */
function box_cors_admin_form($form, &$form_state) {
  //set general warning messages
  drupal_set_message(t('<b>Warning!</b> Changing values in the form will affect the box integration. So please do not change the values with out administrator assistance.'), 'warning');

  $form['box_app_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('App Settings'),
  );
  $form['box_app_settings']['box_cors_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Client Id'),
    '#default_value' => variable_get('box_cors_client_id', ''),
    '#description' => t('You can find the Client Id for your Box application on !link.', array(
      '!link' => l(t('your apps page'), 'https://app.box.com/developers/services'),
    ))
  );

  $form['box_app_settings']['box_cors_client_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Client Secret'),
    '#default_value' => variable_get('box_cors_client_secret', ''),
    '#description' => t('You can find the Client Secret for your Box application on !link.', array(
      '!link' => l(t('your apps page'), 'https://app.box.com/developers/services'),
    ))
  );

  $form['box_app_settings']['box_cors_folder_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Folder Id'),
    '#default_value' => variable_get('box_cors_folder_id', ''),
    '#description' => t('Folder Id of the parent folder where files must get uploaded'),
  );

  $form['box_app_settings']['box_cors_upload_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Upload Url'),
    '#default_value' => variable_get('box_cors_upload_url', 'https://upload.box.com/api/2.0/files/content'),
    '#description' => t('Endpoint url for the Box API'),
  );

  $form['box_app_settings']['box_cors_token_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Token Url'),
    '#default_value' => variable_get('box_cors_token_url', 'https://api.box.com/oauth2/token'),
    '#description' => t('Endpoint url for the Box Token Url'),
  );

  $form['box_app_settings']['box_cors_file_url'] = array(
    '#type' => 'textfield',
    '#title' => t('File Url'),
    '#default_value' => variable_get('box_cors_file_url', 'https://api.box.com/2.0/files/[FILE_ID]'),
    '#description' => t('Endpoint url for the Box File Url. Use [FILE_ID] token for replacing the values'),
  );

  $form['box_token_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Token Settings'),
  );

  $form['box_token_settings']['box_cors_access_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Access token'),
    '#default_value' => variable_get('box_cors_access_token', ''),
    '#description' => t('Access token generated. To be used for upload/ view purpose'),
  );

  $form['box_token_settings']['box_cors_refresh_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Refresh token'),
    '#default_value' => variable_get('box_cors_refresh_token', ''),
    '#description' => t('Refresh token generated from box token generator. To be used for generating new access token. <b>Warning! Refresh token will be valid only for 60 days if unused, please change the values accordingly. </b>'),
  );

  return system_settings_form($form);
}

