<?php

/**
 * @file
 * Drupal stream wrapper implementation for Box File System.
 *
 * Implements DrupalPublicStreamWrapper  to provide an Box wrapper
 * using the "box://" scheme. It can optionally take over for the "public://"
 * stream wrapper, too.
 */

/**
 * The stream wrapper class.
 *
 * In the docs for this class, anywhere you see "<scheme>", it can mean either
 * "box" or "public", depending on which stream is currently being serviced.
 */

class BoxStreamWrapper extends  DrupalPublicStreamWrapper  {
  public function getDirectoryPath() {
    return 'https://api.box.com/2.0/files';
  }

  /**
   * Overrides getExternalUrl().
   *
   * Minimally modified DrupalPublicStreamWrapper::getExternalUrl() to call
   * $this->uriTarget() instead of file_uri_target().
   *
   * Return the HTML URI of a public file.
   */
  function getExternalUrl() {
    $basedir = self::getDirectoryPath();
    $path = str_replace('\\', '/', self::uriTarget($this->uri, $basedir));
    return "$basedir/$path";
  }

  /**
   * Returns the target of a URI (e.g. a stream).
   *
   * @param $uri
   *   A stream, referenced as "scheme://target".
   * @return
   *   A string containing the target (path), or FALSE if none.
   *   For example, the URI "public://sample/test.txt" would return
   *   "sample/test.txt".
   */
  function uriTarget($uri, $basedir) {
    $filepath = file_uri_target($uri);

    // If all we got is hash://directory or even just hash:// then we can't
    // really continue.
    if (!$filepath || strpos($filepath, '.') === FALSE) {
      return $filepath;
    }

    return "$filepath";
  }
}
