This module allows users to generate access token dynamically and
allows users to upload files to box.com through CORS method with out
uploading the files to local file system.

===================
    INSTALLATON
===================
This module requires a Box Application to be created from
https://app.box.com/developers/services. Please specify the urls of hte website
in the configuration settings.

After that CORS support needs to be enabled for the App by raising a support
ticket at https://support.box.com/hc/en-us/requests/new.

Please generate the access and refresh tokens from
http://box-token-generator.herokuapp.com/ while updating the configurations
for the first time. Dont forget change the redirect uri to above url in the
OAUTH2 parameter section of your App in box. Please regenerate tokens if
unauthorized access or any other ajax related errors are coming.

===================
       SETUP
===================
For configuring your module to accept CORS uploads, go to the
admin/config/media/box_cors page on your admin site, fill in the details
related the application and folder id and submit it.

To enable the CORS Upload widget, create a new file field (or edit an existing
one) with the "Box CORS File Upload" widget. This will switch out the usual file
upload mechanism provided by Drupal with one that looks the same, but sends
uploaded files directly to Box, rather than sending them to your Drupal server
first.

===================
   Known Issues
===================
CORS uploading is not supported on older browsers. Your users must use
Internet Explorer 10+ (or Edge), Chrome 30+, Firefox 28+, or Safari 7+.
